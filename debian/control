Source: scythestat
Maintainer: Steffen Moeller <moeller@debian.org>
Section: libs
Priority: optional
Build-Depends: debhelper (>= 11~)
Standards-Version: 4.1.3
Vcs-Browser: https://salsa.debian.org/science-team/scythestat.git
Vcs-Git: https://salsa.debian.org/science-team/scythestat.git
Homepage: http://scythe.wustl.edu

Package: libscythestat-dev
Architecture: all
Section: libdevel
Depends: ${misc:Depends}
Recommends: g++ | c++-compiler
Description: header files for Scythe statistics library
 Files provided by this package are required to develop new programs
 with the Scythe library. There is no binary library associated
 with these headers, i.e. all template code is contained within
 and no further dependencies are required at runtime
 .
 The Scythe Statistical Library is an open source C++ library for
 statistical computation. It includes a suite of matrix manipulation
 functions, a suite of pseudo-random number generators, and a suite
 of numerical optimizers. Programs written using Scythe are generally
 much faster than those written in commonly used interpreted
 languages, such as R, Matlab, and GAUSS; and can be compiled on any
 system with the GNU GCC compiler (and perhaps with other C++
 compilers). One of the primary design goals of the Scythe developers
 has been ease of use for non-expert C++ programmers. Ease of use is
 provided through three primary mechanisms: (1) operator and function
 over-loading, (2) numerous pre-fabricated utility functions, and (3)
 clear documentation and example programs. Additionally, Scythe is
 quite flexible and entirely extensible because the source code is
 available to all users.
